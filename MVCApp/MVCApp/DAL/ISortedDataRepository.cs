﻿using MVCApp.Models;

namespace MVCApp.DAL;

public interface ISortedDataRepository : IRepository<SortedDataModel>
{
    Task<IEnumerable<SortedDataModel>> GetAllAsync();
    Task<SortedDataModel> GetSortedDataByIdAsync(int? id);
    void CreateSortedData(SortedDataModel sortedData);
    void DeleteSortedData(SortedDataModel sortedData);

    SortedDataModel OrderSequence(string cookieValue, string sortType);

    Task<IEnumerable<SortedDataModel>> OrderSequences(string sortType, string searchString);

    int NumberOfSortedData();

    bool CanExport();

    Task<(byte[] fileBytes, string fileName, string mimeType)> CreatedExportFile();
}