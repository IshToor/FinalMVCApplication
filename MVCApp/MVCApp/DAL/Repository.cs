﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using MVCApp.Data;

namespace MVCApp.DAL;

public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
{
    protected MVCAppContext Context { get; set; }

    protected DbSet<TEntity> DbSet;

    public Repository(MVCAppContext context)
    {
        Context = context;
        DbSet = context.Set<TEntity>();
    }

    public async Task<IEnumerable<TEntity>> GetAll()
    {
        return await DbSet.AsNoTracking().ToListAsync();
    }

    public async Task<IEnumerable<TEntity>> GetByCondition(Expression<Func<TEntity, bool>> expression)
    {
        return await DbSet.Where(expression).AsNoTracking().ToListAsync();
    }

    public void Add(TEntity entity)
    {
        DbSet.Add(entity);
    }

    public void Delete(TEntity entityToDelete)
    {
        if (Context.Entry(entityToDelete).State == EntityState.Detached)
        {
            DbSet.Attach(entityToDelete);
        }
        DbSet.Remove(entityToDelete);
    }

    public async Task Delete(int id)
    {
        TEntity entityToDelete = await DbSet.FindAsync(id);
        Delete(entityToDelete);
    }
}