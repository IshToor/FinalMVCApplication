﻿namespace MVCApp.DAL;

public interface IUnitOfWork
{
    public ISortedDataRepository SortedDataRepository { get; }
    Task Save();
    void Dispose();
    Task<bool> CanConnectToDatabase();
}