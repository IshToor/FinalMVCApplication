﻿using System.Linq.Expressions;

namespace MVCApp.DAL;

public interface IRepository<TEntity> where TEntity : class
{
    Task<IEnumerable<TEntity>> GetAll();
    Task<IEnumerable<TEntity>> GetByCondition(Expression<Func<TEntity, bool>> expression);
    void Add(TEntity entity);
    void Delete(TEntity entity);
    Task Delete(int id);
}