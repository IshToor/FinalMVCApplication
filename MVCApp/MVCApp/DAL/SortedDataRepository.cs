﻿using System.Diagnostics;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MVCApp.Data;
using MVCApp.Models;
using Newtonsoft.Json.Linq;

namespace MVCApp.DAL;

public class SortedDataRepository : Repository<SortedDataModel>, ISortedDataRepository
{
    public SortedDataRepository(MVCAppContext context) : base(context)
    {
    }

    public async Task<IEnumerable<SortedDataModel>> GetAllAsync()
    {
        return await GetAll();
    }

    public async Task<SortedDataModel> GetSortedDataByIdAsync(int? id)
    {
        if (!id.HasValue) return null;
        return await DbSet.FirstOrDefaultAsync(x=> x.Id == id.Value);
    }

    public void CreateSortedData(SortedDataModel sortedData)
    {
        Add(sortedData);
    }

    public void DeleteSortedData(SortedDataModel sortedData)
    {
        Delete(sortedData);
    }

    public SortedDataModel OrderSequence(string cookieValue, string sortType)
    {
        var numberSequence = cookieValue.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
            
        var watch = new Stopwatch();
        
        watch.Start();

        numberSequence = sortType == "Ascending" 
            ? numberSequence.OrderBy(x => x).ToList()
            : numberSequence.OrderByDescending(x => x).ToList();
        
        watch.Stop();

        var timeElapsed = watch.Elapsed.TotalMilliseconds + "ms";

        var orderedSequence = string.Join(',',numberSequence);
        
        var sortedDataModel = new SortedDataModel
        {
            SortedNumberSequence = orderedSequence,
            SortType = sortType,
            TimeElapsed = timeElapsed
        };

        return sortedDataModel;
    }

    public async Task<IEnumerable<SortedDataModel>> OrderSequences(string sortType, string searchString)
    {
        IEnumerable<SortedDataModel> sortedSequences = await GetAllAsync();
        if (!String.IsNullOrEmpty(searchString))
        {
            sortedSequences = await GetByCondition(
                x => x.SortedNumberSequence != null 
                     && x.SortedNumberSequence.Contains(searchString));
        }
        
        return sortType == "Ascending"
            ? sortedSequences.OrderBy(x => x.TimeElapsed)
            : sortedSequences.OrderByDescending(x => x.TimeElapsed);
    }

    public int NumberOfSortedData()
    {
        return GetAll().Result.Count();
    }

    public bool CanExport()
    {
        if (NumberOfSortedData() > 0) return true;
        return false;
    }

    public async Task<(byte[] fileBytes, string fileName, string mimeType)> CreatedExportFile()
    {
        const string fileName = "NumberSequences.json";
        const string mimeType = "application/json";
        await using var createStream = System.IO.File.Create(fileName);
            
        var sortedDataModels = await GetAllAsync();

        var dataModels = new JArray(sortedDataModels.Select(x => new JObject
        {
            {"SortedNumberSequence", x.SortedNumberSequence},
            {"SortType", x.SortType},
            {"TimeElapsed", x.TimeElapsed}
   
        }));

        var fileBytes = Encoding.UTF8.GetBytes(dataModels.HasValues ? dataModels.ToString() : string.Empty);
        return (fileBytes, fileName, mimeType);
    }
}