﻿using MVCApp.Data;

namespace MVCApp.DAL;

public class UnitOfWork : IUnitOfWork
{
    private MVCAppContext _context;

    private ISortedDataRepository _sortedDataRepository;
    
    private bool _disposed;


    public ISortedDataRepository SortedDataRepository
    {
        get
        {
            if (_sortedDataRepository == null)
            {
                _sortedDataRepository = new SortedDataRepository(_context);
            }

            return _sortedDataRepository;
        }
    }

    public UnitOfWork(MVCAppContext context)
    {
        _context = context;
    }

    public async Task Save()
    {
        await _context.SaveChangesAsync();
    }
    
    protected virtual void Dispose(bool disposing)
    {
        if (!_disposed)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
        _disposed = true;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    public Task<bool> CanConnectToDatabase()
    {
        return _context.Database.CanConnectAsync();;
    }
}