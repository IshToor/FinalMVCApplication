﻿namespace MVCApp.ViewModels;

public class SortedDataViewModel
{
    public string? SortedNumberSequence { get; set; }

    public string? SortType { get; set; }

    public string? TimeElapsed { get; set; }
}