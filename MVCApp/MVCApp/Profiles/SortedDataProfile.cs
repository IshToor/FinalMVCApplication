﻿using AutoMapper;
using MVCApp.Models;
using MVCApp.ViewModels;

namespace MVCApp.Profiles;

public class SortedDataProfile : Profile
{
    public SortedDataProfile()
    {
        CreateMap<SortedDataModel, SortedDataViewModel>()
            .ForMember(dest => 
                dest.SortedNumberSequence, 
                opt => opt.MapFrom(src => src.SortedNumberSequence))
            .ForMember(dest => 
                    dest.SortType, 
                opt => opt.MapFrom(src => src.SortType))
            .ForMember(dest => 
                    dest.TimeElapsed, 
                opt => opt.MapFrom(src => src.TimeElapsed)).ReverseMap();
    }
}