﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Serialization;

namespace MVCApp.Models;

public class FormNumberInputModel
{
    [Required(ErrorMessage = "Field cannot be empty")]
    [Range(int.MinValue, int.MaxValue, ErrorMessage = "Number is outside of range")] 
    public float CurrentNumber { get; set; }
    public string? SortType { get; set; }
}