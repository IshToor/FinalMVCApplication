using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MVCApp.ActionFilters;
using MVCApp.DAL;
using MVCApp.Models;
using MVCApp.ViewModels;

namespace MVCApp.Controllers
{
    public class NumberSequencesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        
        public IUnitOfWork UnitOfWork { get; private set; }

        private readonly string _cookieName = "NumberCookie";

        public NumberSequencesController(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            UnitOfWork = _unitOfWork;
        }

        public IActionResult Index()
        {
            ViewBag.Message = TempData["SubmissionMessage"] ?? TempData["ErrorMessage"] ?? "";
            return View();
        }

        [DatabaseConnectionActionFilter(nameof(Index))]
        public async Task<IActionResult> Details(int? id)
        {
            var sortedData = await Get(id);
            if (sortedData == null)
            {
                TempData["ErrorMessage"] = "Failed to show details of sorted data";
                return RedirectToAction(nameof(Index));
            }

            var sortedDataViewModel = _mapper.Map<SortedDataViewModel>(sortedData);
            
            return View(sortedDataViewModel);
        }
        public async Task<IActionResult> Delete(int? id)
        {
            var sortedData = await Get(id);
            if (sortedData == null)
            {
                TempData["ErrorMessage"] = "Failed to find sorted data with ID";
                return RedirectToAction(nameof(Index));
            }

            return View(sortedData);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [DatabaseConnectionActionFilter(nameof(Index))]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sortedData = await Get(id);
            if (sortedData == null)
            {
                TempData["ErrorMessage"] = "Failed to delete sorted data with provided ID";
                return RedirectToAction(nameof(Index));
            }

            try
            {
                _unitOfWork.SortedDataRepository.DeleteSortedData(sortedData);
                await _unitOfWork.Save();
            }
            catch (DbUpdateException e)
            {
                TempData["ErrorMessage"] = $"Failed to delete sorted data: {e}";
            }
            
            return RedirectToAction(nameof(ViewSequences));
        }

        [DatabaseConnectionActionFilter(nameof(Index))]
        public async Task<IActionResult> ViewSequences()
        {
            ViewBag.Message = TempData["ErrorMessage"] ?? "";
            return View(await _unitOfWork.SortedDataRepository.GetAllAsync());
        }

        [HttpGet("NumberSequences/ViewSequences/{sortType}")]
        [DatabaseConnectionActionFilter(nameof(Index))]
        public async Task<IActionResult> ViewSequences(string sortType, string searchString)
        {
            ViewBag.Message = TempData["ErrorMessage"] ?? "";
            ViewData["CurrentSort"] = sortType = String.IsNullOrEmpty(sortType) ? "Ascending" : sortType;
            ViewData["CurrentFilter"] = searchString;

            return View(await _unitOfWork.SortedDataRepository.OrderSequences(sortType, searchString));
        }

        public IActionResult SubmitNumber(float currentNumber)
        {
            var roundedToInt = (int)Math.Round(currentNumber);
            var cookieResult = GetCookie(_cookieName);
            if (cookieResult.cookieExists)
            {
                UpdateCookie(_cookieName, cookieResult.cookieValue + "," + roundedToInt);
            }
            else
            {
                UpdateCookie(_cookieName, roundedToInt.ToString());
            }

            return RedirectToAction(nameof(Index));
        }
        
        [HttpPost]
        public async Task<IActionResult> SubmitAll(string sortType)
        {
            if (!ModelState.IsValid) TempData["SubmissionMessage"] = "Submission failed";
            var cookieResult = GetCookie(_cookieName);

            if (!cookieResult.cookieExists)
            {
                TempData["SubmissionMessage"] = "Failed to submit sequence";
                return RedirectToAction(nameof(Index));
            }
            
            var sortedDataModel =
                _unitOfWork.SortedDataRepository
                    .OrderSequence(cookieResult.cookieValue, sortType);

            try
            {
                _unitOfWork.SortedDataRepository.Add(sortedDataModel);
                await _unitOfWork.Save();
                TempData["SubmissionMessage"] = "Submission Successful";
                HttpContext.Response.Cookies.Delete(_cookieName);
            }
            catch (DbUpdateException e)
            {
                TempData["SubmissionMessage"] = $"Submission failed: {e}";
            }
            
            return RedirectToAction(nameof(Index));
        }
        
        [DatabaseConnectionActionFilter(nameof(Index))]
        public async Task<IActionResult> ExportAllSequences()
        {
            if (!_unitOfWork.SortedDataRepository.CanExport()) 
            {
                TempData["ErrorMessage"] = "No data to export";
                return RedirectToAction(nameof(ViewSequences));
            }

            var file = await _unitOfWork.SortedDataRepository.CreatedExportFile();

            return new FileContentResult(file.fileBytes, file.mimeType)
            {
                FileDownloadName = file.fileName
            };
        }

        private async Task<SortedDataModel> Get(int? id)
        {
            return await _unitOfWork.SortedDataRepository.GetSortedDataByIdAsync(id);
        }
        
        private (bool cookieExists, string? cookieValue) GetCookie(string sequenceCookie)
        {
            return (HttpContext.Request.Cookies.TryGetValue(sequenceCookie, out var cookieValue), cookieValue);
        }

        private void UpdateCookie(string? cookie, string value)
        {
            HttpContext.Response.Cookies.Append(cookie, value);
        }

        protected override void Dispose(bool disposing)
        {
            _unitOfWork.Dispose();
            base.Dispose(disposing);
        }
    }
}
