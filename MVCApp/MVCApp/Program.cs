using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MVCApp.ActionFilters;
using MVCApp.Data;
using MVCApp.DAL;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<MVCAppContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("MVCAppContext") ?? throw new InvalidOperationException("Connection string 'MVCAppContext' not found.")));

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped<DatabaseConnectionActionFilter>(_ => new DatabaseConnectionActionFilter(""));

builder.Services.AddAutoMapper(typeof(Program));

// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=NumberSequences}/{action=Index}/{id?}");

app.Run();
