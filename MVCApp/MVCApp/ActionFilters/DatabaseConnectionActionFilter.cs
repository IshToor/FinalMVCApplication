﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using MVCApp.Controllers;

namespace MVCApp.ActionFilters;

public class DatabaseConnectionActionFilter : Attribute, IAsyncActionFilter
{
    private readonly string _redirectToActionName;
    public DatabaseConnectionActionFilter(string redirectToActionName)
    {
        _redirectToActionName = redirectToActionName;
    }
    
    public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
    {
        var controller = context.Controller as NumberSequencesController;
        if (controller != null && !await controller.UnitOfWork.CanConnectToDatabase())
        {
            controller.TempData["ErrorMessage"] = "Cannot connect to database";
            controller.ViewBag.Message = controller.TempData["ErrorMessage"] ?? "";

            context.Result = controller.RedirectToAction(_redirectToActionName);
            return;
        }

        await next();
    }
}