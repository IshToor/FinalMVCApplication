using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MVCApp.Controllers;
using MVCApp.DAL;
using MVCApp.Data;
using MVCApp.Profiles;

namespace UnitTestsProject;

public class UnitTest1
{
    private NumberSequencesController _numberSequencesController;

    private IMapper _mapper;

    private IUnitOfWork _unitOfWork;
    
    private MVCAppContext _context;

    private string connectionString = "Server=localhost\\DATABASE1;Database=database1;Trusted_Connection=True;MultipleActiveResultSets=true;TrustServerCertificate=True";

    public UnitTest1()
    {
        var dbContextOptions = new DbContextOptionsBuilder<MVCAppContext>().UseSqlServer(connectionString).Options;
        _context = new MVCAppContext(dbContextOptions);

        var mappingConfig = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new SortedDataProfile());
        });
        
        _mapper = mappingConfig.CreateMapper();

        _unitOfWork = new UnitOfWork(_context);
        
        _numberSequencesController = new NumberSequencesController(_mapper, _unitOfWork);
    }
    
    [Fact]
    public async void CheckValidAndInvalidData()
    {
        var invalidSortedData = await _numberSequencesController.UnitOfWork.SortedDataRepository.GetSortedDataByIdAsync(-1);
        var validSortedData = await _numberSequencesController.UnitOfWork.SortedDataRepository.GetSortedDataByIdAsync(1);

        Assert.Null(invalidSortedData);
        Assert.NotNull(validSortedData);
    }

    [Fact]
    public async void CheckIdNotModified()
    {
        var validSortedData = await _numberSequencesController.UnitOfWork.SortedDataRepository.GetSortedDataByIdAsync(1);

        Assert.Equal(1, validSortedData.Id);
    }
}